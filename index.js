function Pokemon(name, level) {
	//properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level

	// methods
	
	this.tackle = function(target) {
		
		target.health -= this.attack
		
		console.log(this.name + " tackled " + target.name)
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));

	if (target.health <=5 ){
	 	target.faint()
	 }
	},
	
	this.faint = function () {
		console.warn(this.name + "fainted.")
	}
}


let pikachu = new Pokemon("Pikachu", 50)
let charizard = new Pokemon("Charizard", 75)
let articuno = new Pokemon("Articuno", 100)
console.log(pikachu)
console.log(charizard)

pikachu.tackle(charizard)
articuno.tackle(charizard)

/*
1.)Create a new set of pokemon for pokemon battle. (Same as our discussion)
    - Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
    (target.health - this.attack)

2.) If health is below 5, invoke faint function.

*/